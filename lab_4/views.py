from django.shortcuts import render
from lab_4.forms import NoteForm
from lab_2.models import Note
from django.http.response import HttpResponseRedirect
# Create your views here.
def index(request):
    notes = Note.objects.all()  # TODO Implement this
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)


def addnote (request):

    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            notes = Note.objects.all()  
            response = {'notes': notes}
            return render(request, 'lab4_index.html', response)
    return render(request, "lab4_forms.html")

def note_list(request):
    notes = Note.objects.all()  # TODO Implement this
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)
