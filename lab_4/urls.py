from django.urls import path
from .views import addnote, index, note_list
urlpatterns = [
    path('', index),
    path('add', addnote),
    path('note-list', note_list)
]