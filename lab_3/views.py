from lab_1.models import Friend
from .forms import FriendForm
from django.shortcuts import render
from django.contrib.auth.decorators import login_required  
@login_required(login_url='/admin/login/')

def index(request):
    friends = Friend.objects.all()  
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)


@login_required(login_url='/admin/login/')

def tambahfriend (request):

    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            friends = Friend.objects.all()  
            response = {'friends': friends}
            return render(request, 'lab3_index.html', response)
    return render(request, "lab3_forms.html")
