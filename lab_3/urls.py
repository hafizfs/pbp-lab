from django.urls import path
from .views import index,tambahfriend

urlpatterns = [
    path('', index, name='index'),
    path('add',tambahfriend)
    # TODO Add friends path using friend_list Views
]
