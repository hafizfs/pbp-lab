Apakah perbedaan antara JSON dan XML?
Apakah perbedaan antara HTML dan XML?

Jawab :
1. 
Sebelum mengetahui perbedaan antara XML dan JSON, kita perlu mengetahui apa itu JSON dan XML? 
XML merupakan singkatan dari “Extensive Markup Language” yang merupakan bahasa markup untuk mengkode data terstruktur dan dapat dibaca oleh mesin atau manusia. XML dirancang untuk meningkatkan keter

JSON singkatan dari “JavaScript Object Notation” merupakan format pertukaran data berbasis teks menggunakan tipe data teks dan angka untuk merepresentasikan objek. Berikut merupakan list perbedaan antara JSON dan XML
JSON:
Format berupa data interchange
Data-oriented
Digunakan untuk mengirim data melalui internet agar kemudian dapat diparse
Tidak terlalu berat dan lebih cepat
Menggunakan struktur data map
Sering digunakan untuk pengiriman data antara server dan browser
JSON tidak memiliki tag awal dan akhir 
Memperbolehkan penggunaan array
Hanya mendukung tipe data teks dan numerik

XML:
Format berupa markup language
Document-oriented
Digunakan untuk menstrukturkan data agar dapat menganotasikan metadata dan memparsenya
Menggunakan lebih banyak kosakata untuk mendeskripsikan tujuan
Menggunakan struktur data tree
Sering digunakan untuk menyimpan informasi
XML membutuhkan banyak karakter untuk mewakili data yang sama
Tidak mendukung array
Mendukung berbagai tipe data seperti teks, numerik, gambar, bagan, dll.

2. 
XML dan HTML memiliki kegunaan yang sama yaitu untuk aplikasi web serta pertukaran data. Perbedaan di antara kedua bahasa pemrograman tersebut adalah
HTML lebih fokus pada bagaimana format tampilan dari data sedangkan XML lebih fokus pada struktur dan konteksnya. Meskipun begitu XML dan HTML dapat
saling melengkapi satu sama lain.

HTML:
markup language
tidak case sensitif
Berfungsi ganda sebagai bahasa presentasi.
Memiliki tag yang telah ditentukan sendiri.
Tag penutup tidak selalu diperlukan.
white space tidak di preserved.
Menampilkan desain halaman web dengan cara ditampilkan di sisi klien.
Digunakan untuk menampilkan data.
Null value is natively recognised.
Static

XML :
bahasa markup standar yang mendefinisikan bahasa markup lainnya.
case sensitif
Bukan bahasa presentasi atau bahasa pemrograman.
Tag didefinisikan sesuai kebutuhan programmer. XML fleksibel karena tag dapat ditentukan saat diperlukan
Tag penutup digunakan secara wajib.
mampu preserving white space
Memungkinkan transportasi data dari database dan aplikasi terkait.
Digunakan untuk mentransfer data.
Dynamic