from django.urls import path
from .views import index, xml, json

urlpatterns = [
    path('', index),
    # TODO Add friends path using friend_list Views
    path('xml', xml),
    path('json', json),
]
