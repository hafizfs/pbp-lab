from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.
def index(request):
    notes = Note.objects.all()  # TODO Implement this
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

def xml(request): #Berfungsi untuk mendapat file bersifat XML
    notes = Note.objects.all() 
    data = serializers.serialize('xml', notes) #Diganti notes.objects.all karena yang di panggil notes
    return HttpResponse(data, content_type="application/xml")

def json(request):#Berfungsi untuk mendapat file bersifat json
    notes = Note.objects.all() 
    data = serializers.serialize('json', notes) #Diganti notes.objects.all karena yang di panggil notes
    return HttpResponse(data, content_type="application/json")