from django.db import models

# Create your models here.
class Note(models.Model):
    dari = models.TextField(max_length=128)
    untuk= models.TextField(max_length=128)
    title = models.TextField(max_length=128)
    message = models.TextField(max_length=128)